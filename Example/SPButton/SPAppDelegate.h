//
//  SPAppDelegate.h
//  SPButton
//
//  Created by spily on 07/10/2020.
//  Copyright (c) 2020 spily. All rights reserved.
//

@import UIKit;

@interface SPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
