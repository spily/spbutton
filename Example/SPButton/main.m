//
//  main.m
//  SPButton
//
//  Created by spily on 07/10/2020.
//  Copyright (c) 2020 spily. All rights reserved.
//

@import UIKit;
#import "SPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SPAppDelegate class]));
    }
}
