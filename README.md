# SPButton

[![CI Status](https://img.shields.io/travis/spily/SPButton.svg?style=flat)](https://travis-ci.org/spily/SPButton)
[![Version](https://img.shields.io/cocoapods/v/SPButton.svg?style=flat)](https://cocoapods.org/pods/SPButton)
[![License](https://img.shields.io/cocoapods/l/SPButton.svg?style=flat)](https://cocoapods.org/pods/SPButton)
[![Platform](https://img.shields.io/cocoapods/p/SPButton.svg?style=flat)](https://cocoapods.org/pods/SPButton)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

SPButton is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'SPButton'
```

## Author

spily, 525069689@qq.com

## License

SPButton is available under the MIT license. See the LICENSE file for more info.
